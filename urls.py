# -*- coding: utf-8 -*-
print "~~~~~~~~    urls.py"

from django.conf.urls.defaults import *
from django.contrib import admin
from django.conf import settings
from dtutorial import polls


from cms.sitemaps import CMSSitemap # это карта сайта
admin.autodiscover()


urlpatterns = patterns('',
    # Example:
    # (r'^testsite/', include('testsite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    
    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    
        # подключим опросы
    url(r'^polls/', include('polls.urls')),
    
    # теперь добавим карту сайта http://django-cms.readthedocs.org/en/latest/advanced/sitemap.html
    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {'cmspages': CMSSitemap}}),
    # так подключим cms 
    url(r'^', include('cms.urls')),
    
    
)


if settings.DEBUG:
    urlpatterns = patterns('',
        (r'^' + settings.MEDIA_URL.lstrip('/'), include('appmedia.urls')),
    ) + urlpatterns