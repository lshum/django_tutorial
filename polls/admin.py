﻿# -*- coding: utf-8 -*-
print "~~~~~~~~    polls/admin.py"
from dtutorial.polls.models import Poll
from dtutorial.polls.models import Choice
from django.contrib import admin

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    list_display = ('id', 'question', 'pub_date', 'was_published_today', 'how_many_answers')
    inlines = [ChoiceInline]
    list_filter = ['pub_date']
    search_fields = ['question', 'id']
    date_hierarchy = 'pub_date'

admin.site.register(Poll, PollAdmin)




#admin.site.register(Choice)