# -*- coding: utf-8 -*-
print "~~~~~~~~    polls/models.py"
from django.db import models
import time, datetime


class Poll(models.Model):
    question = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def was_published_today(self):
        return self.pub_date.date() == datetime.date.today()
    def __unicode__(self):
        return self.question
    def how_many_answers(self):
        return self.choice_set.count()

class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice = models.CharField(max_length=200)
    votes = models.IntegerField()
    def __unicode__(self):
        return self.choice